package net.bashtech.plugins.twitchsubwhitelist;

import org.bukkit.scheduler.BukkitRunnable;

public class ReloadTask extends BukkitRunnable {

    private final TwitchSubWhitelist plugin;

    public ReloadTask(TwitchSubWhitelist plugin) {
        this.plugin = plugin;
    }

    public void run() {
        plugin.getLogger().info("Reloading whitelist.");

        if(plugin.getRemoteWhitelist())
            plugin.getLogger().info("Whitelist reloaded.");
        else
            plugin.getLogger().info("Error reloading whitelist.");
    }

}