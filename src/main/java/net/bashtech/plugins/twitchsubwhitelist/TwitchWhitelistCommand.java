package net.bashtech.plugins.twitchsubwhitelist;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

public class TwitchWhitelistCommand implements CommandExecutor {
    private final TwitchSubWhitelist plugin;

    public TwitchWhitelistCommand(TwitchSubWhitelist parent) {
        plugin = parent;
    }

    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if(command.getName().equalsIgnoreCase("tw")){
            if(args.length > 0){
                if(args[0].equalsIgnoreCase("reload")){
                    if(plugin.getRemoteWhitelist())
                        commandSender.sendMessage("Twitch Whitelist reloaded.");
                    else
                        commandSender.sendMessage("Error reloading Twitch Whitelist.");
                }else if(args[0].equalsIgnoreCase("enable")){
                    plugin.enabled = true;
                    plugin.getConfig().set("Enabled", true);
                    plugin.saveConfig();
                    commandSender.sendMessage("Twitch Whitelist enabled.");
                }else if(args[0].equalsIgnoreCase("disable")){
                    plugin.enabled = false;
                    plugin.getConfig().set("Enabled", false);
                    plugin.saveConfig();
                    commandSender.sendMessage("Twitch Whitelist disabled.");
                }else if(args[0].equalsIgnoreCase("export")){
                    plugin.writeWhitelist();
                    commandSender.sendMessage("Twitch Whitelist written to config directory.");
                }else if(args[0].equalsIgnoreCase("list")){
                    String list = "Users: ";
                    for(String user : plugin.whitelist)
                        list += user +", ";
                    commandSender.sendMessage(list);
                }
            }else{
                commandSender.sendMessage("Syntax: /tw reload, /tw enable, /tw disable, /tw export, /tw list");
            }
        }

        return true;
    }
}
