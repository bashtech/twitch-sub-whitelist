package net.bashtech.plugins.twitchsubwhitelist;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class TwitchSubWhitelist extends JavaPlugin implements Listener {
    String uniqueID = null;
    boolean enabled = false;

    Set<String> whitelist = new HashSet<String>();

    public void onDisable() {}

    public void onEnable() {
        //Save default config
        saveDefaultConfig();

        //Register commands and events
        getServer().getPluginManager().registerEvents(this, this);
        getCommand("tw").setExecutor(new TwitchWhitelistCommand(this));

        //Log
        PluginDescriptionFile pdfFile = this.getDescription();
        getLogger().info( pdfFile.getName() + " version " + pdfFile.getVersion() + " is enabled!" );

        //Check for valid uniqueID
        uniqueID = this.getConfig().getString("UniqueID");
        if(uniqueID == null || uniqueID.equalsIgnoreCase("CHANGEME")){
            getLogger().info("Please set your unique ID in config.yml and restart your server.");
        }else{
            if(getRemoteWhitelist()){
                enabled = this.getConfig().getBoolean("Enabled");
            }

            BukkitTask task = new ReloadTask(this).runTaskTimer(this, 1200*10, 1200*10);
        }
    }

    @EventHandler
    public void onPlayerLogin(PlayerLoginEvent event) {
        if(!enabled)
            return;

        Player player = event.getPlayer();

        if(player.hasPermission("twitchwhitelist.exempt") || player.isOp()){
            event.allow();
            getLogger().info("Allowing exempt " + player.getName());
            return;
        }

        if(!whitelist.contains(player.getName().toLowerCase())){
            event.disallow(PlayerLoginEvent.Result.KICK_WHITELIST, "[TwitchWhiteList] Not on whitelist.");
            getLogger().info("Blocking " + player.getName());
        }else{
            getLogger().info("Allowing " + player.getName());
        }
    }

    void writeWhitelist(){
        File whitelistSave = new File(getDataFolder(), "whitelist.txt");

        if(whitelistSave.exists())
            whitelistSave.delete();

        try {
            whitelistSave.createNewFile();

            FileWriter fstream = new FileWriter(whitelistSave);
            BufferedWriter out = new BufferedWriter(fstream);

            for(String player : whitelist){
                out.write(player + "\n");
            }

            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    boolean getRemoteWhitelist(){
        List<String> temp = new LinkedList<String>();

        String urlString = "http://whitelist.twitchapps.com/list.php?id=" + uniqueID + "&header=CHECK";
        try{
            getLogger().info("Getting whitelist from " + urlString);
            URL url = new URL(urlString);
            HttpURLConnection conn =(HttpURLConnection)url.openConnection();

            try{
                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String inputLine;
                int lineNumber = 1;
                while ((inputLine = in.readLine()) != null){
                    inputLine = inputLine.trim();
                    if(lineNumber == 1 && !inputLine.equals("CHECK"))
                        throw new IOException();

                    temp.add(inputLine.toLowerCase());
                    lineNumber++;
                }
                in.close();

                whitelist.clear();
                for(String player : temp)
                    whitelist.add(player);
            }catch (IOException exerr){
                String inputLine;
                String errorIn = "";
                InputStream errorStream = conn.getErrorStream();
                if(errorStream != null){
                    BufferedReader inE = new BufferedReader(new InputStreamReader(errorStream));
                    while ((inputLine = inE.readLine()) != null)
                        errorIn += inputLine;
                    inE.close();
                }
                getLogger().info("Error getting list: " + errorIn);
            }

            return true;
        }catch(Exception ex){
            ex.printStackTrace();
            return false;
        }

    }
}

